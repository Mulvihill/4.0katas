function sendFunction (func, str) {
    let titleDiv = document.createElement("div");
    titleDiv.textContent = str;
    let dContent = document.createElement("div");
    dContent.textContent = func;
    titleDiv.appendChild(dContent);
    document.body.appendChild(titleDiv);
}

const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function amel (strang, splPar) {
    
    let a = strang.split(splPar);
    return a;
}

sendFunction (amel (gotCitiesCSV, ','), "Katas 1");
sendFunction (amel (bestThing, " "), "Katas 2");
sendFunction (gotCitiesCSV.replace(/,/gi, ';'), "Katas 3")

sendFunction (lotrCitiesArray.join(), "Katas 4");
sendFunction (lotrCitiesArray.slice(0, 5), "Katas 5");
sendFunction (lotrCitiesArray.slice(3), "Katas 6");
sendFunction (lotrCitiesArray.slice(2, 5), "Katas 7");
let dRohan = lotrCitiesArray.splice(2,1);
sendFunction (lotrCitiesArray, "Katas 8");
let marshBattle = lotrCitiesArray.splice(4,3);
sendFunction (lotrCitiesArray, "Katas 9");
lotrCitiesArray.splice(2,0, "Rohan");
sendFunction (lotrCitiesArray, "Katas 10");
lotrCitiesArray.splice(4,0, "Deadest Marsh");
sendFunction (lotrCitiesArray, "Katas 11");

function bestF14 () {
sendFunction (bestThing.slice(0,14), "Katas 12");
}
bestF14();
sendFunction (bestThing.slice(-12), "Katas 13");
sendFunction (bestThing.slice(23,38), "Katas 14");
sendFunction (bestThing.substring(bestThing.length-12), "Katas 15");
sendFunction(bestThing.substring(23,38), "Katas 16");
sendFunction (bestThing.indexOf("only"), "Katas 17");
sendFunction (bestThing.indexOf("bit"), "Katas 18");

let arGot = gotCitiesCSV.split(",");
let dubV = [];
for (let sub in arGot) {
    if (arGot[sub].indexOf("aa") > 0|| arGot[sub].indexOf("ee") > 0 || arGot[sub].indexOf("ii") > 0 || arGot[sub].indexOf("oo") > 0 || arGot[sub].indexOf("uu") > 0) {
        dubV.push(arGot[sub]);
    }
}
sendFunction (dubV, "Katas 19");
let lotrOr = [];
for (let p in lotrCitiesArray) {
    if (lotrCitiesArray[p].substring(lotrCitiesArray[p].length - 2) === 'or') {
        lotrOr.push(lotrCitiesArray[p]);
    }
}
sendFunction (lotrOr, "Katas 20");

let bestArray = bestThing.split(" ");
let newBestArray = [];

for (let w in bestArray) {
    if (bestArray[w].indexOf("b") === 0 ) {
        newBestArray.push(bestArray[w]);
    }
}
sendFunction(newBestArray, "Katas 21");

function mirkwoodStatus () {
    if (lotrCitiesArray.includes("Mirkwood")) {
        return "Yes";
    }
    else {return "No";}
}
sendFunction(mirkwoodStatus(), "Katas 22");

function hollywoodStatus () {
    if (lotrCitiesArray.includes("Hollywood")) {
        return "Yes";
    } else {return "No";}
}
sendFunction(hollywoodStatus(), "Katas 23");



function findMirk (mirk) {
    return mirk === "Mirkwood";
}

sendFunction(lotrCitiesArray.findIndex(findMirk), "Katas 24");

let spaceLotr = [];

for (let m in lotrCitiesArray) {
    if (lotrCitiesArray[m].indexOf(" ") > 0) {
        spaceLotr.push(lotrCitiesArray[m]);
    }
}
sendFunction(spaceLotr, "Katas 25");

sendFunction(lotrCitiesArray.reverse(), "Katas 26");

sendFunction(lotrCitiesArray.sort(), "Katas 27");

function sortLength (a, b) {
    return (a.length - b.length);
}

sendFunction(lotrCitiesArray.sort(sortLength), "Katas 28");

var dCity = lotrCitiesArray.pop();
sendFunction(lotrCitiesArray, "Katas 29");
lotrCitiesArray.push(dCity);
sendFunction(lotrCitiesArray, "Katas 30");

function arShift (arr) {
    var sepArr = arr.shift();
    return "Original: " + " " + arr +  "Shifted: " + " " + sepArr;
}

sendFunction(arShift(lotrCitiesArray), "Katas 31");
lotrCitiesArray.unshift("Rohan");
sendFunction(lotrCitiesArray, "Katas 32");